package com.infomil.terminauxmobile.testcalculatrice

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

class CalculaticeTestApplication : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_calculatice_test_application)
    }
}